package com.galvanize.Domain;

public class AuthenicationResponse {
    private boolean authenticated;
    private ReturnUser user;

    public AuthenicationResponse(boolean authenticated,ReturnUser user){
        this.authenticated = authenticated;
        this.user = user;
    }


    public boolean isAuthenticated() {
        return authenticated;
    }

    public ReturnUser getUser() {
        return user;
    }
}
