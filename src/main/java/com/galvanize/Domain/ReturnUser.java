package com.galvanize.Domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

public class ReturnUser {

    private Long id;
    private String email;
    private String password;

    public ReturnUser(Long id, String email, String password) {
        this.id = id;
        this.email = email;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    @JsonIgnore
    public String getPassword(){
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
