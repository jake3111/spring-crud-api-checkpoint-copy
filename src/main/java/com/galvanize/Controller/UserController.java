package com.galvanize.Controller;

import com.galvanize.Domain.AuthenicationResponse;
import com.galvanize.Domain.ReturnUser;
import com.galvanize.Domain.User;
import com.galvanize.Repository.UserRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserRepository repository;

    public UserController(UserRepository repository) {
        this.repository = repository;
    }

    @GetMapping("")
    public Iterable<User> all() {
        return this.repository.findAll();
    }

    @PostMapping("")
    public ReturnUser create(@RequestBody User user) {
        User user1 = this.repository.save(user);
        return new ReturnUser(user1.getId(), user1.getEmail(), user1.getPassword());
    }

    @GetMapping("/{id}")
    public ReturnUser getUserById(@PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        User user = repository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));
        return new ReturnUser(user.getId(), user.getEmail(), user.getPassword());
    }

    @PatchMapping("{id}")
    public ReturnUser patch(@PathVariable(value = "id") Long userId, @RequestBody Map requestParams) {
        return repository.findById(userId).map(user -> {
            if (requestParams.get("email") != null) {
                user.setEmail((String) requestParams.get("email"));
            }
            if (requestParams.get("password") != null) {
                user.setPassword((String) requestParams.get("password"));
            }
            User user1 = repository.save(user);
            return new ReturnUser(user1.getId(),user1.getEmail(),user1.getPassword());
        }).orElseGet(() -> {
            return null;
        });
    }

    @DeleteMapping("/{id}")
    public Map<String, Long> deleteUser(@PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        User user = repository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("Lesson not found for this id :: " + userId));

        repository.delete(user);
        Map<String, Long> response = new HashMap<>();
        response.put("count", repository.count());
        return response;
    }

    @PostMapping("/authenticate")
    public AuthenicationResponse authenticateUser(@RequestBody Map requestBody)
            throws ResourceNotFoundException {

        Iterator<User> userIterator = repository.findAll().iterator();
        while(userIterator.hasNext()) {
            User localUser = userIterator.next();
            ReturnUser returnUser = new ReturnUser(localUser.getId(), localUser.getEmail(), localUser.getPassword());
            if ((requestBody.get("email") != null && localUser.getEmail().equals(requestBody.get("email"))) &&
                    (requestBody.get("email") != null && localUser.getPassword().equals(requestBody.get("password")))){
                return new AuthenicationResponse(true, returnUser);
            }
        }
        return new AuthenicationResponse(false, null);
    }
}
