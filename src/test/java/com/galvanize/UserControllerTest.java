package com.galvanize;

import com.galvanize.Domain.ReturnUser;
import com.galvanize.Domain.AuthenicationResponse;
import com.galvanize.Repository.UserRepository;
import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.junit.Assert.assertTrue;

import javax.transaction.Transactional;

import java.io.File;
import java.util.*;

import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest {

    @Autowired
    MockMvc mvc;

    @Autowired
    UserRepository userRepository;

    @Test
    @Transactional
    @Rollback
    void testPostThenGetThenPatchThenAuthenicateThenGetAllThenDeleteUser() throws Exception{

        File jsonFile = new File("src/test/resources/test-user-obj.json");
        Scanner myReader = new Scanner(jsonFile);
        StringBuilder sb = new StringBuilder();
        while (myReader.hasNextLine()) {
            sb.append(myReader.nextLine());
        }
        myReader.close();
        ReturnUser expectedUser = new ReturnUser(1L, "john@example.com", null);

        Gson gson = new Gson();

        RequestBuilder request = MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(sb.toString());

        this.mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string(gson.toJson(expectedUser)));

        RequestBuilder getRequest = MockMvcRequestBuilders.get("/users/1");

        this.mvc.perform(getRequest)
                .andExpect(status().isOk())
                .andExpect((content()).string(gson.toJson(expectedUser)));

        File jsonFile1 = new File("src/test/resources/test_patch_user_obj.json");
        Scanner myReader1 = new Scanner(jsonFile1);
        StringBuilder sb1 = new StringBuilder();
        while (myReader1.hasNextLine()) {
            sb1.append(myReader1.nextLine());
        }
        myReader1.close();

        RequestBuilder patchRequest = MockMvcRequestBuilders.patch("/users/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(sb1.toString());

        ReturnUser expectedUser1 = new ReturnUser(1L, "jake@example.com", "something-secret-secret");

        AuthenicationResponse authenticationResponse = new AuthenicationResponse(true, expectedUser1);
        expectedUser1.setPassword(null);

        this.mvc.perform(patchRequest)
                .andExpect(status().isOk())
                .andExpect(content().string(gson.toJson(expectedUser1)));

        //authenticate request
        RequestBuilder authenticateRequest = MockMvcRequestBuilders.post("/users/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(sb1.toString());

        this.mvc.perform(authenticateRequest)
                .andExpect(status().isOk())
                .andExpect(content().string(gson.toJson(authenticationResponse)));

        //get all request
        RequestBuilder getAllRequest = MockMvcRequestBuilders.get("/users/");

        ResultActions response = this.mvc.perform(getAllRequest)
                .andExpect(status().isOk());

        String getAllResponseString = response.andReturn().getResponse().getContentAsString();

        ReturnUser [] expectedReturnUserArray = gson.fromJson(getAllResponseString, ReturnUser[].class);
        assertTrue(expectedReturnUserArray.length == 1);
        //delete request
        RequestBuilder deleteRequest = MockMvcRequestBuilders.delete("/users/1");

        ResultActions deleteResponse = this.mvc.perform(deleteRequest)
                .andExpect(status().isOk());
        String deleteResponseString = deleteResponse.andReturn().getResponse().getContentAsString();
        Map<String, Long> expectedDeleteResponse = new HashMap<>();
        expectedDeleteResponse.put("count", 0L);
        Map deleteResponseMap = gson.fromJson(deleteResponseString, Map.class);

        assertTrue(deleteResponseMap.get("count").equals(0.0));
    }

}
