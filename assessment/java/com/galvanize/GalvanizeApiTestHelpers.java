package com.galvanize;

import com.google.gson.*;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import javax.transaction.Transactional;

@Transactional
@Rollback  // requires spring.jpa.database-platform=org.hibernate.dialect.MySQL5InnoDBDialect
public class GalvanizeApiTestHelpers {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    TestRestTemplate template;

    /*
    This method should not be required if @Rollback is working.
    Unfortunately it is NOT working when run inside docker compose
    testenv container.

    TODO: figure out how to enable @Rollback with docker compose
    */
    @BeforeEach
    public void clearDatabase() {
        this.jdbcTemplate.execute("SET foreign_key_checks = 0;");

        String query = "SELECT table_name FROM information_schema.tables WHERE TABLE_SCHEMA IN (SELECT DATABASE())";
        this.jdbcTemplate.queryForList(query).forEach(row -> {
            this.jdbcTemplate.execute("TRUNCATE TABLE " + row.get("TABLE_NAME"));
        });

        this.jdbcTemplate.execute("SET foreign_key_checks = 1");
    }

    protected JsonElement get(String url) {
        ResponseEntity<String> responseEntity = requestWithoutBody(url, HttpMethod.GET);
        assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.OK));
        return JsonParser.parseString(responseEntity.getBody());
    }

    protected JsonElement post(String url, JsonObject payload) {
        return requestWithBody(url, payload, HttpMethod.POST);
    }

    protected JsonElement patch(String url, JsonObject payload) {
        return requestWithBody(url, payload, HttpMethod.PATCH);
    }

    protected JsonElement delete(String url) {
        ResponseEntity<String> responseEntity = requestWithoutBody(url, HttpMethod.DELETE);
        return JsonParser.parseString(responseEntity.getBody());
    }

    protected ResponseEntity<String> requestWithoutBody(String url, HttpMethod method) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(headers);

        return template.exchange(url, method, entity, String.class);
    }

    protected JsonElement requestWithBody(String url, JsonObject payload, HttpMethod method) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        Gson builder = new GsonBuilder().create();
        String jsonString = builder.toJson(payload);

        HttpEntity<String> requestEntity = new HttpEntity<>(jsonString, headers);
        ResponseEntity<String> responseEntity = template.exchange(url, method, requestEntity, String.class);

        assertThat(responseEntity.getStatusCode(), equalTo(HttpStatus.OK));
        return JsonParser.parseString(responseEntity.getBody());
    }
}
