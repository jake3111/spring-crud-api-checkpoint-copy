package com.galvanize;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableWithSize.iterableWithSize;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
@Rollback
public class AssessmentTest extends GalvanizeApiTestHelpers {

    @Test
    public void testCreate() throws Exception {
        JsonObject user = new JsonObject();
        String email = getRandomEmail();

        JsonObject payload = new JsonObject();
        payload.add("email", JsonParser.parseString(email));
        payload.add("password", JsonParser.parseString("password"));

        String url = "/users";
        JsonObject createdUser = post(url, payload).getAsJsonObject();
        assertThat(createdUser.get("email").getAsString(), equalTo(email));

        ensurePasswordIsNotPresent(createdUser);

        JsonArray userList = get(url).getAsJsonArray();

        Long idFromCreate = createdUser.get("id").getAsLong();
        Long idFromIndex = userList.get(0).getAsJsonObject().get("id").getAsLong();
        assertThat(idFromCreate, equalTo(idFromIndex));

        userShouldBeAuthenticated(email, "password");
        userShouldNotBeAuthenticated(email, "wrong password");
    }

    @Test
    public void testShow() throws Exception {

        JsonObject payload = new JsonObject();
        payload.add("email", JsonParser.parseString(getRandomEmail()));
        payload.add("password", JsonParser.parseString("password"));

        JsonObject createdUser = post("/users", payload).getAsJsonObject();
        String idFromCreate = createdUser.get("id").getAsString();

        JsonObject showUser = get("/users/" + idFromCreate).getAsJsonObject();

        assertThat(showUser.get("id").getAsString(), equalTo(idFromCreate));
        assertThat(showUser.get("email").getAsString(), equalTo(payload.get("email").getAsString()));
    }

    @Test
    public void testUpdateWithoutPassword() throws Exception {
        String password = "pass1234";
        String originalEmail = getRandomEmail();
        String newEmail = getRandomEmail();

        JsonObject payload = new JsonObject();
        payload.addProperty("email", originalEmail);
        payload.addProperty("password", password);

        JsonObject createdUser = post("/users", payload).getAsJsonObject();
        String idFromCreate = createdUser.get("id").getAsString();

        JsonObject payload1 = new JsonObject();
        payload1.addProperty("id", idFromCreate);
        payload1.addProperty("email", newEmail);

        JsonObject patchUser = patch("/users/" + idFromCreate, payload1).getAsJsonObject();
        ensurePasswordIsNotPresent(patchUser);

        JsonObject showUser = get("/users/" + idFromCreate).getAsJsonObject();
        ensurePasswordIsNotPresent(showUser);

        assertThat(showUser.get("id").getAsString(), equalTo(idFromCreate));
        assertThat(showUser.get("email").getAsString(), equalTo(newEmail));

        userShouldBeAuthenticated(newEmail, password);
        userShouldNotBeAuthenticated(originalEmail, password);
    }

    @Test
    public void testUpdateWithPassword() throws Exception {
        String email = getRandomEmail();
        String originalPassword = "1234pass";
        String newPassword = "abc123";

        JsonObject user = new JsonObject();
        user.addProperty("email", email);
        user.addProperty("password", originalPassword);

        JsonObject payload = new JsonObject();
        payload.add("user", user);

        JsonObject createdUser = post("/users", payload).getAsJsonObject();
        String idFromCreate = createdUser.get("id").getAsString();

        user.addProperty("id", idFromCreate);
        user.addProperty("email", email);
        user.addProperty("password", newPassword);

        JsonObject updatedUser = patch("/users/" + idFromCreate, user).getAsJsonObject();
        ensurePasswordIsNotPresent(updatedUser);

        assertThat(updatedUser.get("id").getAsString(), equalTo(idFromCreate));
        assertThat(updatedUser.get("email").getAsString(), equalTo(email));

        userShouldBeAuthenticated(email, newPassword);
        userShouldNotBeAuthenticated(email, originalPassword);
    }

    @Test
    public void testDelete() throws Exception {
        String email = getRandomEmail();
        String password = "1234";

        JsonObject user = new JsonObject();
        user.addProperty("email", email);
        user.addProperty("password", password);

        JsonObject payload = new JsonObject();
        payload.add("user", user);

        JsonObject createdUser = post("/users", payload).getAsJsonObject();
        String idFromCreate = createdUser.get("id").getAsString();

        JsonObject response = delete("/users/" + idFromCreate).getAsJsonObject();
        int count = response.get("count").getAsInt();
        assertThat(count, equalTo(0));

        JsonArray userList = get("/users").getAsJsonArray();

        assertThat(userList.size(), equalTo(0));

        userShouldNotBeAuthenticated(email, password);
    }

    private String getRandomEmail() {
        return "user" + String.valueOf(new Random().nextInt()) + "@example.com";
    }

    private void ensurePasswordIsNotPresent(JsonObject createdUser) {
        List<String> keys = createdUser.entrySet()
                .stream()
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        assertThat(keys, iterableWithSize(2));
        assertThat(keys, containsInAnyOrder("id", "email"));
    }

    private JsonObject authenticate(String email, String password) {
        JsonObject credentials = new JsonObject();
        credentials.addProperty("email", email);
        credentials.addProperty("password", password);

        return post("/users/authenticate", credentials).getAsJsonObject();
    }

    private void userShouldBeAuthenticated(String email, String password) {
        JsonObject response = authenticate(email, password);
        assertThat(response.get("authenticated").getAsBoolean(), equalTo(true));

        JsonObject user = response.get("user").getAsJsonObject();
        ensurePasswordIsNotPresent(user);
    }

    private void userShouldNotBeAuthenticated(String email, String password) {
        JsonObject response = authenticate(email, password);
        assertThat(response.get("authenticated").getAsBoolean(), equalTo(false));
    }

}
