# CRUD API Assessment: Users

## Setup
Gradle version 6.3 or above is required for this assessment. 

In order to run locally, create two databases:

```
mysql -uroot -e "create database spring_crud_users;"
mysql -uroot -e "create database spring_crud_users_test;"
```

To run the app:

```
./gradlew bootRun
```

To run the tests:

```
./assess-project
```
**NOTE**

If your username or password is different, then you'll need to ignore all of the application.properties files and add your credentials.

